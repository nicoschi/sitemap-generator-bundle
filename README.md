# Nicoschi Sitemap Bundle

A Symfony bundle to generate xml sitemaps from JSON endpoints through a command line.  

## Installation

### Step 1: Add repository to composer.json

Open your project `composer.json` and add a new repository in repositories list in order to download the bundle through composer: 

```json
"repositories": [
  {
      "type": "vcs",
      "url": "https://bitbucket.org/nicoschi/sitemap-generator-bundle.git"
  }
]
```

### Step 2: Download the Bundle

Open a command console, enter your project directory and execute the following command to download the latest stable version of this bundle:

```console
$ composer require nicoschi/sitemap-generator-bundle
```

This command requires you to have Composer (version >= 1.8) installed globally, as explained in the [installation chapter](https://getcomposer.org/doc/00-intro.md) of the Composer documentation.

### Step 3: Enable the Bundle

#### Symfony 4

Enable the bundle by adding it to the list of registered bundles in the `config/bundles.php` file of your project:

```php
// config/bundles.php

return [
    // ...
    Nicoschi\SitemapBundle\NicoschiSitemapBundle::class => ['all' => true],
];
```

#### Symfony 3

Enable the bundle by adding it to the list of registered bundles in the `app/AppKernel.php` file of your project:

```php
public function registerBundles()
{
    $bundles = [
        // ...
        new \Nicoschi\SitemapBundle\NicoschiSitemapBundle(),
    ];
}
```

### Step 4: Configure the bundle

#### Symfony 4

In `config/packages` folder add a new yaml file named `nicoschi_sitemap.yaml`

#### Symfony 3

Append the configuration in `app/config/config.yml`

#### Configuration example

Given a json data response example from https://example.com/first_endpoint like this: 

```json
{
  "info": {
    "count": 4
  },
  "object": {
    "id": 1,
    "data": [
      {
        "key": 1,
        "content": "content of node 1",
        "url": {
          "title": "node 1",
          "value": "http://example.com/it/node_1"
        }
      },
      {
        "key": 2,
        "content": "content of node 2",
        "url": {
          "title": "node 2",
          "value": "http://example.com/it/node_2"
        }
      }
    ]
  }
}
```

configure the bundle as follow:

```yaml
nicoschi_sitemap:
  # required - must be an existing folder in project and a file name ending in .xml
  sitemap_path: '%kernel.project_dir%/public/sitemap.xml'
  # optional - max urls contained in each sitemap. default value 50000
  max_urls: 50000
  # required - an endpoint at least
  endpoints:
    first_endpoint:
      # required - must be an url
      url: 'https://example.com/first_endpoint'
      # optional - use it to pass an array of options to Guzzle get http request 
      # (see: http://docs.guzzlephp.org/en/stable/request-options.html) 
      get_request_options:
        headers:
          Accept-Language: 'it-IT'
      # optional - the json response from the endpoint is transformed in an associative array 
      # so use this to indicate through an array of keys the level of the associative array to loop. 
      # Given a json response as the example above if you want to loop the "data" children 
      # use the following value
      root_node: ['object', 'data']
      # required - must contain an url. Use this to indicate through an array of keys 
      # the element during the loop which contain the url for the xml sitemap loc. 
      # Given a json response as the example above and given you are looping 
      # ['object', 'data'] use the following configuration
      sitemap_loc_key: ['url', 'value']
      # optional - must be a float between 0.0 and 1.0
      priority: 0.5
      # optional - must be a value among 'always', 'hourly', 'daily', 'weekly', 'monthly', 'yearly', 'never'
      change_frequency: 'always'
      # optional - use it if some of the following endpoints urls need data from this one
      related_endpoints:
          # key of the following endpoint related with this one
          second_endpoint:
            # array of relation keys
            relation_keys:
              # use the key to indicate the url paramater of the following endpoint 
              # to replace and the value to indicate through an array of keys where 
              # the replacing value is taken
              # Given a json response as the example above and given you are looping 
              # ['object', 'data'] use the following configuration 
              url_parameter_to_replace: ['key']
    second_endpoint:
      # this url will be replaced by https://example.com/second_endpoint/{value of first_endpoint.object.data.key}
      # during the loop of the element from the first endpoinf
      url: 'https://example.com/second_endpoint/url_parameter_to_replace'
      sitemap_loc_key: ['url']
```

#### Usage

Run `bin/console nicoschi:create-sitemap` command to generate sitemaps from endpoints listed in the configuration.
 


