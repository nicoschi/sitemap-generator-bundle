<?php

namespace Nicoschi\SitemapBundle\Command;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use samdark\sitemap\Sitemap;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SitemapGeneratorCommand
 * @package Nicoschi\SitemapBundle\Command
 */
class SitemapGeneratorCommand extends Command {

  protected static $defaultName = 'nicoschi:create-sitemap';

  /**
   * @var ClientInterface
   */
  protected $httpClient;

  /**
   * @var Sitemap
   */
  protected $sitemap;

  /**
   * @var array
   */
  protected $config;

  /**
   * @var array
   */
  protected $endpoints;

  /**
   * @var int
   */
  protected $timestamp;

  /**
   * @var OutputInterface
   */
  protected $output;

  /**
   * SitemapGeneratorCommand constructor.
   * @param array $config
   */
  public function __construct(array $config) {
    $this->config = $config;
    $this->endpoints = $config['endpoints'];

    parent::__construct();
  }

  /**
   * @inheritdoc
   */
  protected function configure() {
    $this
      ->setDescription('Generate sitemaps from json endpoints')
      ->setHelp('Configure the Nicoschi SitemapBundle before using the command. See https://bitbucket.org/nicoschi/sitemap-generator-bundle/src/master/');
  }

  /**
   * @inheritdoc
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $this->output = $output;

    $this->httpClient = new Client();

    $this->sitemap = new Sitemap($this->config['sitemap_path']);
    $this->sitemap->setMaxUrls($this->config['max_urls']);

    $this->writeBlock(['Sitemap generation started ...'], 'info');

    foreach ($this->endpoints as $key => &$endpoint) {
      $this->addToSitemap($key, $endpoint);
    }

    $this->sitemap->write();

    if($this->listSitemapPaths()) {
      $this->writeBlock(['Sitemap generated'], 'info');
    }
  }

  /**
   * @param string $key
   * @param array $endpoint
   */
  protected function addToSitemap(string $key, array $endpoint) {
    $getRquestOptions = isset($endpoint['get_request_options']) ? $endpoint['get_request_options'] : array();

    $response = $this->httpClient->request('get', $endpoint['url'], $getRquestOptions);

    $json_data = json_decode($response->getBody(), TRUE);

    if(!empty($json_data)) {
      if(!empty($endpoint['root_node'])) {
        $json_data = $this->delimitArray($json_data, $endpoint['root_node']);
      }

      foreach ($json_data as $data) {
        try {
          $sitemapLoc = $this->delimitArray($data, $endpoint['sitemap_loc_key']);

          $this->sitemap->addItem(
            $sitemapLoc,
            $this->getTimestamp(),
            isset($endpoint['change_frequency']) ? $endpoint['change_frequency'] : NULL,
            isset($endpoint['priority']) ? $endpoint['priority'] : NULL
          );
        }
        catch(\Exception $exception) {
          $this->writeBlock(['Error from endpoint ' . $key . ': ' . $exception->getMessage()], 'error');
        }

        if(!empty($endpoint['related_endpoints'])) {
          foreach ($endpoint['related_endpoints'] as $id => $relatedEndpoint) {
            $endpoint_rel = $this->endpoints[$id];

            foreach ($relatedEndpoint['relation_keys'] as $k => $relationKey) {
              $endpoint_rel['url'] = str_replace($k, $this->delimitArray($data, $relationKey), $endpoint_rel['url']);
            }

            $this->addToSitemap($id, $endpoint_rel);
          }
        }
      }

      if(isset($id)) {
        unset($this->endpoints[$id]);
      }
    }
  }

  /**
   * @param array $array
   * @param array $delimiter
   * @return mixed
   */
  protected function delimitArray(array $array, array $delimiter) {
    $value = $array[$delimiter[0]];
    array_shift($delimiter);

    if (empty($delimiter)) {
      return $value;
    }

    return $this->delimitArray($value, $delimiter);
  }

  /**
   * @return int
   */
  protected function getTimestamp() {
    if(!isset($this->timestamp)) {
      $this->timestamp = time();
    }

    return $this->timestamp;
  }

  /**
   * @return bool
   */
  protected function listSitemapPaths() {
    $writtenFilePaths = $this->sitemap->getWrittenFilePath();

    if(empty($writtenFilePaths)) {
      $this->writeBlock(['Something went wrong, any sitemap has been generated'], 'error');
      return FALSE;
    }

    array_unshift($writtenFilePaths, 'This are the generated sitemap files: ');
    $this->writeBlock($writtenFilePaths, 'info');
    return TRUE;
  }

  /**
   * @param array $message
   * @param string $type
   */
  protected function writeBlock(array $message, string $type) {
    $formattedBlock = $this->getHelper('formatter')->formatBlock($message, $type);
    $this->output->writeln($formattedBlock);
  }

}