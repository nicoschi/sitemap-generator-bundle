<?php

namespace Nicoschi\SitemapBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class NicoschiSitemapBundle
 * @package Nicoschi\SitemapBundle
 */
class NicoschiSitemapBundle extends Bundle {

}