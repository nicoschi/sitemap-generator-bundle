<?php

namespace Nicoschi\SitemapBundle\DependencyInjection;

use Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Class NicoschiSitemapExtension
 * @package Nicoschi\SitemapBundle\DependencyInjection
 */
class NicoschiSitemapExtension extends Extension {

  /**
   * @inheritdoc
   * @throws Exception
   */
  public function load(array $configs, ContainerBuilder $container) {
    $configuration = new Configuration();
    $config = $this->processConfiguration($configuration, $configs);

    $loader = new YamlFileLoader($container, new FileLocator(dirname(__DIR__).'/Resources/config'));
    $loader->load('services.yaml');

    $container->setParameter('nicoschi_sitemap', $config);
  }

}