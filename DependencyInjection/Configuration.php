<?php

namespace Nicoschi\SitemapBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 * @package Nicoschi\SitemapBundle\DependencyInjection
 */
class Configuration implements ConfigurationInterface {
  public function getConfigTreeBuilder() {
    $treeBuilder = new TreeBuilder('nicoschi_sitemap');
    $rootNode = $treeBuilder->root('nicoschi_sitemap');

    $rootNode
      ->children()
        ->scalarNode('sitemap_path')
          ->isRequired()
          ->cannotBeEmpty()
          ->validate()
            ->ifTrue(function($value) {
              return !(substr($value, -4) === '.xml');
            })
            ->thenInvalid('File extension in sitemap_path must be .xml')
          ->end()
        ->end()
        ->integerNode('max_urls')
          ->defaultValue(50000)
        ->end()
        ->arrayNode('endpoints')
          ->isRequired()
          ->cannotBeEmpty()
          ->arrayPrototype()
            ->children()
              ->scalarNode('url')
                ->isRequired()
                ->cannotBeEmpty()
              ->end()
              ->arrayNode('get_request_options')
                ->variablePrototype()->end()
              ->end()
              ->arrayNode('root_node')
                ->cannotBeEmpty()
                ->scalarPrototype()->end()
              ->end()
              ->arrayNode('sitemap_loc_key')
                ->isRequired()
                ->cannotBeEmpty()
                ->scalarPrototype()->end()
              ->end()
              ->floatNode('priority')
                ->min(0.0)->max(1.0)
              ->end()
              ->enumNode('change_frequency')
                ->values(['always', 'hourly', 'daily', 'weekly', 'monthly', 'yearly', 'never'])
              ->end()
              ->arrayNode('related_endpoints')
                ->cannotBeEmpty()
                ->arrayPrototype()
                  ->children()
                    ->arrayNode('relation_keys')
                      ->cannotBeEmpty()
                      ->variablePrototype()
                        ->isRequired()
                        ->cannotBeEmpty()
                        ->validate()
                          ->ifTrue(function($value) {
                            return !is_array($value);
                          })
                          ->thenInvalid('Must be a non empty array')
                        ->end()
                      ->end()
                    ->end()
                  ->end()
                ->end()
              ->end()
            ->end()
          ->end()
        ->end()
      ->end()
    ->end();

    return $treeBuilder;
  }

}